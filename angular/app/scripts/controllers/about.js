'use strict';

/**
 * @ngdoc function
 * @name yatzeecoukApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the yatzeecoukApp
 */
angular.module('yatzeecoukApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

'use strict';

/**
 * @ngdoc function
 * @name yatzeecoukApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the yatzeecoukApp
 */
angular.module('yatzeecoukApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
